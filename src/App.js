import React, { Component } from "react";
import logo from "./logo.svg";
import { connect } from "react-redux";
import "./App.css";
import axios from "axios";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = { name: "", password: "" };
    this.handleChange = this.handleChange.bind(this);
    this.login = this.login.bind(this);
  }

  handleChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }

  async login() {
    try {
      const response = await axios.get("https://api.github.com/zen");
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>Edit src/App.js and save to reload.</p>
          <form>
            <div>
              <input
                class="input is-primary"
                type="text"
                name="name"
                onChange={this.handleChange}
                placeholder="Username"
              />
            </div>
            <input
              class="input is-warning"
              type="password"
              name="password"
              onChange={this.handleChange}
              placeholder="Password"
            />
            <a class="button is-primary" onClick={() => this.login()}>
              TEKAN
            </a>
          </form>
          <p>
            {this.state.name} || {this.state.password}
          </p>
        </header>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  test: state.player.player.name
});

export default connect(
  mapStateToProps,
  null
)(App);
