export const GET_PLAYER = "GET_PLAYER";
export const GET_PLAYER_SUCCESS = "GET_PLAYER_SUCCESS";
export const GET_PLAYER_FAIL = "GET_PLAYER_FAIL";

export function getPlayer(firstName, surname) {
  return {
    type: GET_PLAYER,
    payload: { firstName, surname }
  };
}

const initialState = {
  player: { name: "hoi" },
  _loading: false,
  error: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_PLAYER:
      return {
        ...state,
        _loading: true
      };
    case GET_PLAYER_SUCCESS:
      return {
        ...state,
        player: { ...action.payload },
        _loading: false
      };
    case GET_PLAYER_FAIL:
      return {
        ...state,
        _loading: false,
        error: action.payload
      };
    default:
      return state;
  }
}
